import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:responsive_view/model/navigation_item.dart';
import 'package:responsive_view/model/responsive_model.dart';
import 'package:responsive_view/widget/inherited_responsive_widget.dart';

class ResponsiveView extends StatefulWidget {
  final Widget navigationLeading;
  final Widget navigationTitle;
  final List<NavigationItem> navigationItems;
  final Widget? Function(BuildContext context,
      {Key? scaffoldKey, Widget? endDrawer}) builder;

  const ResponsiveView({
    Key? key,
    required this.navigationLeading,
    required this.navigationTitle,
    required this.navigationItems,
    required this.builder,
  }) : super(key: key);

  @override
  ResponsiveViewState createState() => ResponsiveViewState();
}

class ResponsiveViewState extends State<ResponsiveView>
    with TickerProviderStateMixin {
  BuildContext? _context;
  Widget? _endDrawer;
  TabController? _controller;

  @override
  void initState() {
    super.initState();
    _controller =
        TabController(length: widget.navigationItems.length, vsync: this);
    if (widget.navigationItems.any((element) => element.selected)) {
      final _index =
          widget.navigationItems.lastIndexWhere((element) => element.selected);
      _controller?.index = _index;
    }
  }

  @override
  void didUpdateWidget(covariant ResponsiveView oldWidget) {
    super.didUpdateWidget(oldWidget);
    final eq = const ListEquality().equals;
    if (!eq(oldWidget.navigationItems, widget.navigationItems)) {
      _controller =
          TabController(length: widget.navigationItems.length, vsync: this);
      if (widget.navigationItems.any((element) => element.selected)) {
        setState(() {
          final _index = widget.navigationItems
              .lastIndexWhere((element) => element.selected);
          _controller?.index = _index;
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Builder(builder: (context) {
      return Scaffold(
        endDrawer: _endDrawer,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          elevation: 0,
          centerTitle: false,
          title: LayoutBuilder(builder: (context, constraints) {
            return Container(
              child: ConstrainedBox(
                constraints: BoxConstraints(
                  maxHeight: 60,
                  maxWidth: constraints.maxWidth,
                ),
                child: Stack(
                  children: [
                    if (constraints.maxWidth < 460 &&
                        showFirstControlButton()) ...[
                      Positioned(
                        top: 12,
                        child: IconButton(
                          onPressed: () {
                            setState(() {
                              _controller?.index -= 1;
                            });
                            widget.navigationItems[_controller!.index].onTap();
                          },
                          icon: Icon(Icons.arrow_back),
                        ),
                      )
                    ],
                    if (constraints.maxWidth < 460 &&
                        showLastControlButton()) ...[
                      Positioned(
                        top: 12,
                        right: 0,
                        child: IconButton(
                          onPressed: () {
                            setState(() {
                              _controller?.index += 1;
                            });
                            widget.navigationItems[_controller!.index].onTap();
                          },
                          icon: Icon(Icons.arrow_forward),
                        ),
                      )
                    ],
                    Positioned(
                      top: 12,
                      left: constraints.maxWidth < 460 ? 60 : 0,
                      right: constraints.maxWidth < 460 ? 60 : 0,
                      child: TabBar(
                        controller: _controller,
                        unselectedLabelColor:
                            Theme.of(context).colorScheme.onSurface,
                        splashBorderRadius: BorderRadius.circular(16),
                        labelPadding: const EdgeInsets.symmetric(vertical: 4),
                        indicator: BoxDecoration(
                            color: Theme.of(context).colorScheme.primary,
                            borderRadius: BorderRadius.circular(16)),
                        isScrollable: true,
                        tabs: [
                          ...widget.navigationItems.map(
                            (item) {
                              return ConstrainedBox(
                                constraints: BoxConstraints(
                                    minWidth: 150, maxWidth: 350),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    (item.leading ?? const SizedBox()),
                                    const SizedBox(
                                      width: 8,
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 2),
                                      child: item.title,
                                    ),
                                    if (item.trailing != null) ...[
                                      const SizedBox(
                                        width: 8,
                                      ),
                                      (item.trailing ?? const SizedBox()),
                                    ]
                                  ],
                                ),
                              );
                            },
                          ),
                        ],
                        labelStyle: TextStyle(
                          color: Theme.of(context).colorScheme.onPrimary,
                        ),
                        labelColor: Theme.of(context).colorScheme.onPrimary,
                        onTap: (index) {
                          widget.navigationItems[index].onTap.call();
                        },
                      ),
                    ),
                  ],
                ),
              ),
            );
          }),
          actions: const [
            SizedBox(),
          ],
        ),
        body: Builder(builder: (context) {
          _context = context;
          return widget.builder.call(context, endDrawer: _endDrawer) ??
              const SizedBox();
        }),
      );
    });
  }

  bool showFirstControlButton() {
    return _controller?.index != 0;
  }

  bool showLastControlButton() {
    return _controller?.index != widget.navigationItems.length - 1;
  }

  void openEndDrawer(Widget? endDrawer) {
    setState(() {
      _endDrawer = endDrawer;
    });
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      Scaffold.of(_context ?? context).openEndDrawer();
    });
  }

  void closeEndDrawer() {
    Scaffold.of(_context ?? context).closeEndDrawer();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      setState(() {
        _endDrawer = null;
      });
    });
  }
}
