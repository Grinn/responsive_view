import 'package:flutter/material.dart';
import 'package:responsive_view/model/responsive_action.dart';
import 'package:responsive_view/widget/action_button.dart';

class DesktopView extends StatelessWidget {
  final Key? scaffoldKey;
  final Widget navigationLeading;
  final Widget navigationTitle;
  final List<Widget> navigationItems;
  final Widget? contentLeading;
  final Widget? contentTitle;
  final PreferredSizeWidget? contentBottom;
  final List<ResponsiveAction>? actions;
  final Widget? child;
  final Widget? Function(BuildContext context,
      {Key? scaffoldKey, Widget? endDrawer})? builder;
  final Widget? endDrawer;

  const DesktopView({
    Key? key,
    this.scaffoldKey,
    required this.navigationLeading,
    required this.navigationTitle,
    required this.navigationItems,
    required this.contentTitle,
    this.contentLeading,
    this.contentBottom,
    this.actions,
    this.child,
    this.builder,
    this.endDrawer,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Flexible(
          flex: 0,
          child: ConstrainedBox(
            constraints: const BoxConstraints(maxWidth: 250),
            child: Scaffold(
              appBar: AppBar(
                automaticallyImplyLeading: false,
                elevation: 0,
                shape: Border(
                  top: BorderSide(color: Theme.of(context).dividerColor),
                  bottom: BorderSide(color: Theme.of(context).dividerColor),
                  right: BorderSide(color: Theme.of(context).dividerColor),
                ),
                leading: navigationLeading,
                centerTitle: false,
                title: navigationTitle,
              ),
              body: Container(
                decoration: BoxDecoration(
                  border: Border(
                      right: BorderSide(color: Theme.of(context).dividerColor)),
                ),
                child: ListTileTheme(
                  selectedTileColor:
                      Theme.of(context).primaryColor.withOpacity(0.1),
                  child: ListView(
                    primary: false,
                    controller: ScrollController(),
                    children: navigationItems,
                  ),
                ),
              ),
            ),
          ),
        ),
        if (builder != null) ...[
          Flexible(
              child: builder?.call(context,
                      scaffoldKey: scaffoldKey, endDrawer: endDrawer) ??
                  const SizedBox())
        ],
        if (builder == null) ...[
          Flexible(
            child: ScaffoldMessenger(
              child: Scaffold(
                key: scaffoldKey,
                endDrawer: endDrawer,
                appBar: AppBar(
                  automaticallyImplyLeading: false,
                  elevation: 0,
                  shape: contentLeading != null ||
                          contentTitle != null ||
                          contentBottom != null ||
                          (actions != null && actions!.isNotEmpty)
                      ? Border(
                          top:
                              BorderSide(color: Theme.of(context).dividerColor),
                          bottom:
                              BorderSide(color: Theme.of(context).dividerColor),
                        )
                      : null,
                  leading: contentLeading,
                  bottom: contentBottom,
                  centerTitle: false,
                  title: Row(children: [
                    Padding(
                      padding: const EdgeInsets.only(right: 40.0),
                      child: contentTitle,
                    ),
                    ...actions
                            ?.map((action) => ActionButton(
                                title: action.title,
                                onPressed: action.onPressed,
                                icon: action.icon))
                            .toList() ??
                        []
                  ]),
                ),
                body: child,
              ),
            ),
          ),
        ],
      ],
    );
  }
}
