import 'package:flutter/material.dart';
import 'package:responsive_view/model/responsive_action.dart';
import 'package:responsive_view/widget/action_button.dart';

class TabletView extends StatefulWidget {
  final Widget navigationLeading;
  final Widget navigationTitle;
  final List<Widget> navigationItems;
  final Widget? contentLeading;
  final Widget? contentTitle;
  final PreferredSizeWidget? contentBottom;
  final List<ResponsiveAction>? actions;
  final Widget? child;
  final Widget? Function(BuildContext context)? builder;

  const TabletView({
    Key? key,
    required this.navigationLeading,
    required this.navigationTitle,
    required this.navigationItems,
    required this.contentTitle,
    this.contentLeading,
    this.contentBottom,
    this.actions,
    this.child,
    this.builder,
  }) : super(key: key);

  @override
  _TabletViewState createState() => _TabletViewState();
}

class _TabletViewState extends State<TabletView> {
  int _visibleActions = 0;
  final _widgetKey = GlobalKey();
  Size? _spacerSize;

  @override
  void initState() {
    super.initState();
    _visibleActions = widget.actions?.length ?? 0;
  }

  @override
  Widget build(BuildContext context) {
    Future.delayed(const Duration(milliseconds: 300), () {
      postFrameCallback(context);
    });
    return NotificationListener(
      onNotification: (notification) {
        if (notification is SizeChangedLayoutNotification) {
          Future.delayed(const Duration(milliseconds: 300), () {
            postFrameCallback(context);
          });
        }
        return true;
      },
      child: SizeChangedLayoutNotifier(
        child:  ScaffoldMessenger(
            child: Scaffold(
              appBar: AppBar(
                automaticallyImplyLeading: false,
                elevation: 0,
                shape: widget.contentLeading != null ||
                        widget.contentTitle != null ||
                        widget.contentBottom != null ||
                        (widget.actions != null && widget.actions!.isNotEmpty)
                    ? Border(
                        bottom:
                            BorderSide(color: Theme.of(context).dividerColor))
                    : null,
                leading: widget.contentLeading,
                bottom: widget.contentBottom,
                centerTitle: false,
                title: Builder(
                  builder: (context) {
                    if (widget.actions?.length != null) {
                      return Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(right: 40.0),
                            child: widget.contentTitle,
                          ),
                          ...widget.actions!
                              .take(_visibleActions)
                              .map((action) => ActionButton.fromAction(action)),
                          Spacer(key: _widgetKey),
                          if (widget.actions!.length > _visibleActions)
                            PopupMenuButton(
                                icon: Icon(Icons.more_vert,
                                    color: Theme.of(context).iconTheme.color),
                                itemBuilder: (context) => widget.actions!
                                    .skip(_visibleActions)
                                    .map((action) => PopupMenuItem(
                                        child: ActionButton.fromAction(action)))
                                    .toList())
                        ],
                      );
                    }
                    return Row(children: [
                      Padding(
                        padding: const EdgeInsets.only(right: 40.0),
                        child: widget.contentTitle,
                      ),
                      ...widget.actions
                              ?.map((action) => ActionButton.fromAction(action))
                              .toList() ??
                          []
                    ]);
                  },
                ),
              ),
              body: Column(
                children: [
                  Container(height: 20, color: Colors.red,), 
                  (widget.builder?.call(context) ?? widget.child ?? const SizedBox()),
                ],
              ),
            ),
          ),
        ),
    );
  }

  void postFrameCallback(_) {
    final context = _widgetKey.currentContext;
    if (context == null) return;

    final newSize = context.size;
    if (_spacerSize == newSize) return;

    _spacerSize = newSize;

    if (_spacerSize!.width <= 30) {
      setState(() {
        _visibleActions--;
      });
      postFrameCallback(_);
    } else if (_spacerSize!.width > 200) {
      setState(() {
        if (_visibleActions < (widget.actions?.length ?? 0)) {
          _visibleActions++;
        }
      });
      postFrameCallback(_);
    }
  }
}
