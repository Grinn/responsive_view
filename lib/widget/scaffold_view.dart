import 'package:flutter/material.dart';
import 'package:responsive_view/model/responsive_action.dart';

class ScaffoldView extends StatelessWidget {
  final Key? scaffoldKey;
  final Widget navigationLeading;
  final Widget navigationTitle;
  final List<Widget> navigationItems;
  final Widget? contentLeading;
  final PreferredSizeWidget? contentBottom;
  final List<ResponsiveAction>? actions;
  final Widget? child;
  final Widget? Function(BuildContext context,
      {Key? scaffoldKey, Widget? endDrawer}) builder;
  final Widget? endDrawer;

  const ScaffoldView({
    Key? key,
    this.scaffoldKey,
    required this.navigationLeading,
    required this.navigationTitle,
    required this.navigationItems,
    required this.builder,
    this.contentLeading,
    this.contentBottom,
    this.actions,
    this.child,
    this.endDrawer,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return builder.call(context,
            scaffoldKey: scaffoldKey, endDrawer: endDrawer) ?? const SizedBox();
  }
}
