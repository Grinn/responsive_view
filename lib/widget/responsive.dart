import 'package:flutter/widgets.dart';
import 'package:responsive_builder/responsive_builder.dart' hide WidgetBuilder;

typedef ResponsiveTabletBuilder<T> = Widget Function(
    BuildContext context, Widget mobile);
typedef ResponsiveDesktopBuilder<T> = Widget Function(
    BuildContext context, Widget mobile, Widget tablet);

class Responsive<T> extends StatelessWidget {
  final WidgetBuilder mobile;
  final ResponsiveTabletBuilder? tablet;
  final ResponsiveDesktopBuilder? desktop;
  final ScreenBreakpoints? breakpoints;

  const Responsive({
    Key? key,
    required this.mobile,
    this.tablet,
    this.desktop,
    this.breakpoints,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _mobile = mobile(context);
    final _tablet = tablet != null ? tablet!(context, _mobile) : _mobile;
    final _desktop =
        desktop != null ? desktop!(context, _mobile, _tablet) : _mobile;
    return ScreenTypeLayout.builder(
      breakpoints: breakpoints,
      mobile: mobile,
      tablet: (context) => _tablet,
      desktop: (context) => _desktop,
    );
  }
}
