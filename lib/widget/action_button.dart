import 'package:flutter/material.dart';
import 'package:responsive_view/model/responsive_action.dart';

class ActionButton extends StatelessWidget {
  final String title;
  final Widget? child;
  final EdgeInsets? padding;
  final VoidCallback onPressed;
  final IconData? icon;

  const ActionButton({
    Key? key,
    required this.title,
    required this.onPressed,
    this.icon,
    this.child,
    this.padding,
  }) : super(key: key);

  factory ActionButton.fromAction(ResponsiveAction action) {
    return ActionButton(
      title: action.title,
      onPressed: action.onPressed,
      icon: action.icon,
    );
  }

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: onPressed,
      child: Padding(
        padding:
            padding ?? const EdgeInsets.symmetric(vertical: 4, horizontal: 8),
        child: Row(
          children: [
            if (child == null) ...[
              if (icon != null)
                Icon(icon, color: Theme.of(context).colorScheme.primary),
              Text(
                title.toUpperCase(),
                style: Theme.of(context)
                    .textTheme
                    .labelLarge!
                    .copyWith(color: Theme.of(context).colorScheme.primary),
              ),
            ],
            if (child != null) ...[
              child!,
            ],
          ],
        ),
      ),
    );
  }
}
