import 'package:flutter/widgets.dart';
import 'package:responsive_view/model/responsive_model.dart';

class InheritedResponsiveWidget extends InheritedWidget {
  InheritedResponsiveWidget({
    super.key,
    required this.model,
    Widget? child,
  }) : super(child: child ?? const SizedBox());

  final ResponsiveModel model;

  static InheritedResponsiveWidget? maybeOf(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<InheritedResponsiveWidget>();
  }

  static InheritedResponsiveWidget of(BuildContext context) {
    final InheritedResponsiveWidget? result = maybeOf(context);
    assert(result != null, 'No InheritedResponsiveWidget found in context');
    return result!;
  }

  @override
  bool updateShouldNotify(InheritedResponsiveWidget oldWidget) {
    return model.endDrawer != oldWidget.model.endDrawer ||
        model.drawer != oldWidget.model.endDrawer;
  }
}
