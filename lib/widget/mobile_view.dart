import 'package:flutter/material.dart';
import 'package:responsive_view/model/responsive_action.dart';
import 'package:responsive_view/widget/action_button.dart';

class MobileView extends StatefulWidget {
  final Widget navigationLeading;
  final Widget navigationTitle;
  final List<Widget> navigationItems;
  final Widget? contentLeading;
  final Widget? contentTitle;
  final PreferredSizeWidget? contentBottom;
  final List<ResponsiveAction>? actions;
  final Widget? child;
  final Widget? Function(BuildContext context)? builder;

  const MobileView({
    Key? key,
    required this.navigationLeading,
    required this.navigationTitle,
    required this.navigationItems,
    required this.contentTitle,
    this.contentLeading,
    this.contentBottom,
    this.actions,
    this.child,
    this.builder,
  }) : super(key: key);

  @override
  _MobileViewState createState() => _MobileViewState();
}

class _MobileViewState extends State<MobileView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ScaffoldMessenger(
        child: Scaffold(
          appBar: AppBar(
            automaticallyImplyLeading: false,
            elevation: 0,
            shape: widget.contentLeading != null ||
                    widget.contentTitle != null ||
                    widget.contentBottom != null ||
                    (widget.actions != null && widget.actions!.isNotEmpty)
                ? Border(
                    bottom: BorderSide(color: Theme.of(context).dividerColor))
                : null,
            leading: widget.contentLeading,
            bottom: widget.contentBottom,
            centerTitle: false,
            titleSpacing: widget.contentLeading == null ? null : 0,
            title: Builder(
              builder: (context) {
                return Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(right: 40.0),
                      child: widget.contentTitle,
                    ),
                    const Spacer(),
                    if (widget.actions?.isNotEmpty ?? false)
                      PopupMenuButton(
                          icon: Icon(Icons.more_vert,
                              color: Theme.of(context).iconTheme.color),
                          itemBuilder: (context) => widget.actions!
                              // .skip(1)
                              .map((action) => PopupMenuItem(
                                      child: ActionButton(
                                    title: action.title,
                                    icon: action.icon,
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                      action.onPressed();
                                    },
                                  )))
                              .toList())
                  ],
                );
              },
            ),
          ),
          body: widget.builder?.call(context) ?? widget.child,
        ),
      ),
    );
  }
}
