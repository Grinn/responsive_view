import 'package:flutter/material.dart';
import 'package:responsive_view/model/responsive_action.dart';
import 'package:responsive_view/responsive_view.dart';

class ResponsiveScaffold {
  Widget? contentLeading;
  Widget? contentTitle;
  PreferredSizeWidget? contentBottom;
  Widget? child;
  List<ResponsiveAction>? actions;
  Key? scaffoldKey;
  Widget? endDrawer;

  ResponsiveScaffold({
    this.contentLeading,
    this.contentTitle,
    this.contentBottom,
    this.child,
    this.actions,
    this.scaffoldKey,
    this.endDrawer,
  });

  Widget build(BuildContext context) {
    return ScaffoldMessenger(
      child: Scaffold(
        key: scaffoldKey,
        endDrawer: endDrawer,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          elevation: 0,
          shape: contentLeading != null ||
                  contentTitle != null ||
                  contentBottom != null ||
                  (actions != null && actions!.isNotEmpty)
              ? Border(
                  top: BorderSide(color: Theme.of(context).dividerColor),
                  bottom: BorderSide(color: Theme.of(context).dividerColor),
                )
              : null,
          leading: contentLeading,
          bottom: contentBottom,
          centerTitle: false,
          title: Builder(builder: (context) {
            return Row(children: [
              Padding(
                padding: const EdgeInsets.only(right: 40.0),
                child: contentTitle,
              ),
              ...actions
                      ?.map((action) => ActionButton(
                          title: action.title,
                          onPressed: action.onPressed,
                          icon: action.icon))
                      .toList() ??
                  []
            ]);
          }),
          actions: const [
            SizedBox(),
          ],
        ),
        body: Builder(builder: (context) {
          return child ?? const SizedBox();
        }),
      ),
    );
  }
}
