import 'package:flutter/material.dart';

class NavigationItem {
  final Widget title;
  final Widget? leading;
  final Widget? trailing;
  final bool selected;
  final Function() onTap;

  NavigationItem({
    required this.title,
    this.leading,
    this.trailing,
    required this.selected,
    required this.onTap,
  });
}
