import 'package:flutter/material.dart';

class ResponsiveModel {
  Widget? drawer;
  Widget? endDrawer;

  ResponsiveModel({this.drawer, this.endDrawer});

  Future<void> setEndDrawer(Widget? endDrawer, BuildContext context) async {
    this.endDrawer = endDrawer;
    await Future.delayed(const Duration(milliseconds: 300), () {
      Scaffold.of(context).openEndDrawer();
    });
  }
}
