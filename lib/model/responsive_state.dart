import 'package:responsive_view/model/view_destination.dart';

class ResponsiveState<R> {
  final Map<ViewDestination, R> _states;

  const ResponsiveState(this._states);

  void stateForDestination(ViewDestination destination, R state) {
    _states[destination] = state;
  }

  R operator [](ViewDestination item) {
    return _states[item]!;
  }

  void operator []=(ViewDestination item, R state) {
    _states[item] = state;
  }
}
