import 'package:flutter/material.dart';

class ResponsiveAction {
  final String title;
  final IconData? icon;
  final Function() onPressed;

  ResponsiveAction({
    this.icon,
    required this.title,
    required this.onPressed,
  });
}
